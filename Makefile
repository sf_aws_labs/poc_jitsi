include .env
all: plan

install: _terraform _ansible
start:
	# Get Public ip from state

stop:
	echo stop jitsi server
plan:
	@cd terraform && terraform plan --var "ssh_key_url=${ssh_key_url}"
init:
	@cd terraform && terraform init

_terraform:
	cd terraform && terraform apply --auto-approve --var "ssh_key_url=${ssh_key_url}"

_ansible:
	./create_inventory.sh
	cd ansible && ansible-playbook -i inv install.yml