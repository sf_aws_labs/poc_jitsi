#!/bin/sh

cd terraform
# Read state file
IP=$(cat terraform.tfstate | jq -r ".outputs.ip.value")

echo "[all:vars]\nansible_ssh_user=ubuntu\n[all]" > ../ansible/inv 
echo "$IP" >> ../ansible/inv