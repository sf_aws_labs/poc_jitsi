provider "aws"{
    region = "eu-central-1"
}

variable "ssh_key_url" {
    type = "string"
}

variable "ssh_keypair" {
    type = "string"
    default = "private_aws"
}

data "aws_security_group" "launch-wizard" {
    name = "launch-wizard-1"
}

data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-trusty-14.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

resource "aws_instance" "jitsi_server" {
    ami = "${data.aws_ami.ubuntu.id}"
    vpc_security_group_ids = [
        "${data.aws_security_group.launch-wizard.id}"
    ]
    associate_public_ip_address = true
    instance_type = "t3.micro"
    key_name = "${var.ssh_keypair}"
    user_data = <<EOF
curl ${var.ssh_key_url} >> ~/.ssh/authorized_keys
    EOF
}

output "ip" {
  value = "${aws_instance.jitsi_server.public_ip}"
}
